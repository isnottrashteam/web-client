'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:ApresentationCtrl
 * @description
 * # ApresentationCtrl
 * Controller of the clientApp
 */
angular.module('clientApp')
  .controller('ApresentationCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
