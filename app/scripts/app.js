'use strict';

/**
 * @ngdoc overview
 * @name clientApp
 * @description
 * # clientApp
 *
 * Main module of the application.
 */

angular
  .module('clientApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ]).run(function($rootScope, $location) {
    angular.element(document).ready(function ($state) {
        $rootScope.go = function (path) {
          $location.path(path);
        };
    });

  }).config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/apresentation.html',
        controller: 'ApresentationCtrl',
        controllerAs: 'apresentation',
        url: '/'
      })
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl',
        controllerAs: 'login'
      })
      .when('/register', {
        templateUrl: 'views/register.html',
        controller: 'RegisterCtrl',
        controllerAs: 'register'
      })
      .when('/main', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/apresentation', {
        templateUrl: 'views/apresentation.html',
        controller: 'ApresentationCtrl',
        controllerAs: 'apresentation'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
